#!/usr/bin/env bash

cp "/data3/murillo/reference/dmel-all-chromosome-r6.15.fasta" ./dmel_ref.fasta
samtools faidx dmel_ref.fasta
#watch -g ps -opid -p 10666;
screen CRISP --bams bams.txt --ref dmel_ref.fasta --VCF snp_calls_all.vcf 2>&1 calls.log

