#!/usr/bin/env bash

#filtrar cromossomos que n?o interessam
grep -e $'^2L\t' -e $'^2R\t' -e $'^3L\t' -e $'^3R\t' -e $'^4\t' -e $'^X\t' -e "^#" ../chr_calls/snp_calls_all_cat.vcf > tmp1.vcf
#separar SNVs de INDELs
#grep -e "INSERTION" -e "DELETION" tmp1.vcf > indels.vcf
#grep -e "VT=SNV;" tmp1.vcf > tmp2.vcf

#remover SNPs around 5bp indels
l-bcftools filter -g 5 tmp1.vcf > tmp2.vcf
#manter apenas SNVs
grep -e "VT=SNV;" tmp2.vcf > tmp3.vcf

#ajeitar o arquivo do repeatmasker baixado de: http://www.repeatmasker.org/species/dm.html
#sed '1,3d' dm6.fa.out | tr -s ' ' | sed 's/^ //g' | cut -d' ' -f5,6,7 | tr '[:blank:]' \\t | sed 's/chr//g' | bedtools sort > repeat_6.bed
#sort
bedtools sort -i tmp3.vcf | sponge tmp3.vcf
#adicionar o header
head -n 22 tmp1.vcf | cat - tmp3.vcf | sponge tmp3.vcf
bedtools subtract -a tmp3.vcf -b ../../../resources/repeat_6.bed > tmp4.vcf

#recode and maf
recode_maf.py tmp4.vcf 0.0 > tmp5.vcf

#indels
#cut -f1,2 indels.vcf > tmpi.vcf
#rearrange_bed.py tmpi.vcf 5 > indels.bed\
#sortBed -i indels.bed | sponge indels.bed
#head -n 22 tmp1.vcf | cat - tmp4.vcf | sponge tmp4.vcf
#bedtools subtract -a tmp4.vcf -b indels.bed > tmp5.vcf

#dgrp
head -n 22 tmp1.vcf | cat - tmp5.vcf | sponge tmp5.vcf
#cut -f1,2 dgrp2.vcf > dgrp2.bed
#tail -n +8 dgrp2.bed | sponge dgrp2.bed
#awk '{print $1"\t"$2"\t"$2}' dgrp2.bed | sponge dgrp2.bed
#bedtools intersect -u -a tmp5.vcf -b ../../resources/dgrp2_6.bed > snps_filtered.vcf
#estranhamente o bedtools intersect duplicou algumas linhas...
#uniq snvs_nr_maf10_noindels_dgrp.vcf | sponge snvs_nr_maf10_noindels_dgrp.vcf

#mincov
#filter_mincov.py snvs_nr_maf15_noindels_dgrp.vcf > final_snvs.vcf
java -Xmx16G -jar $SnpEff -v dmel_r6.12 -onlyProtein true tmp5.vcf > snps_filtered.ann.vcf
#cat snps_filtered.ann.vcf | vcfEffOnePerLine.pl > oneperline_snps_filtered.ann.vcf
