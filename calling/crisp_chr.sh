#!/usr/bin/env bash

screen CRISP --bams bams.txt --ref dmel_ref.fasta --VCF snp_calls_all_2L_1.vcf --regions 2L:1-6000000 2>&1 2L_1_calls.log
screen CRISP --bams bams.txt --ref dmel_ref.fasta --VCF snp_calls_all_2L_2.vcf --regions 2L:6000001-12000000 2>&1 2L_2_calls.log
screen CRISP --bams bams.txt --ref dmel_ref.fasta --VCF snp_calls_all_2L_3.vcf --regions 2L:12000001-18000000 2>&1 2L_3_calls.log
screen CRISP --bams bams.txt --ref dmel_ref.fasta --VCF snp_calls_all_2L_4.vcf --regions 2L:18000001-23513712 2>&1 2L_4_calls.log

screen CRISP --bams bams.txt --ref dmel_ref.fasta --VCF snp_calls_all_2R_1.vcf --regions 2R:1-6000000 2>&1 2R_1_calls.log
screen CRISP --bams bams.txt --ref dmel_ref.fasta --VCF snp_calls_all_2R_2.vcf --regions 2R:6000001-12000000 2>&1 2R_2_calls.log
screen CRISP --bams bams.txt --ref dmel_ref.fasta --VCF snp_calls_all_2R_3.vcf --regions 2R:12000001-18000000 2>&1 2R_3_calls.log
screen CRISP --bams bams.txt --ref dmel_ref.fasta --VCF snp_calls_all_2R_4.vcf --regions 2R:18000001-25286936 2>&1 2R_4_calls.log

screen CRISP --bams bams.txt --ref dmel_ref.fasta --VCF snp_calls_all_3L_1.vcf --regions 3L:1-6000000 2>&1 3L_1_calls.log
screen CRISP --bams bams.txt --ref dmel_ref.fasta --VCF snp_calls_all_3L_2.vcf --regions 3L:6000001-12000000 2>&1 3L_2_calls.log
screen CRISP --bams bams.txt --ref dmel_ref.fasta --VCF snp_calls_all_3L_3.vcf --regions 3L:12000001-18000000 2>&1 3L_3_calls.log
screen CRISP --bams bams.txt --ref dmel_ref.fasta --VCF snp_calls_all_3L_4.vcf --regions 3L:18000001-24000000 2>&1 3L_4_calls.log
screen CRISP --bams bams.txt --ref dmel_ref.fasta --VCF snp_calls_all_3L_5.vcf --regions 3L:24000001-28110227 2>&1 3L_5_calls.log

screen CRISP --bams bams.txt --ref dmel_ref.fasta --VCF snp_calls_all_3R_1.vcf --regions 3R:1-6000000 2>&1 3R_1_calls.log
screen CRISP --bams bams.txt --ref dmel_ref.fasta --VCF snp_calls_all_3R_2.vcf --regions 3R:6000001-12000000 2>&1 3R_2_calls.log
screen CRISP --bams bams.txt --ref dmel_ref.fasta --VCF snp_calls_all_3R_3.vcf --regions 3R:12000001-18000000 2>&1 3R_3_calls.log
screen CRISP --bams bams.txt --ref dmel_ref.fasta --VCF snp_calls_all_3R_4.vcf --regions 3R:18000001-24000000 2>&1 3R_4_calls.log
screen CRISP --bams bams.txt --ref dmel_ref.fasta --VCF snp_calls_all_3R_5.vcf --regions 3R:24000001-32079331 2>&1 3R_5_calls.log


