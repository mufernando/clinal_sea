#!/usr/bin/env python3

""" This script recodes a VCF from CRISP variant caller to AD:DP format. It
also removes alleles with average minor allele frequency less than the MAF
supplied.
Arguments: MAF
Written by Murillo F. Rodrigues - murillofer.rodrigues@gmail.com"""

import sys

def recode(cols, maf):
    #rename FORMAT
    cols[8] = "AD:DP"
    freqs = []
    #Iterate over samples. Samples start from the 10th column.
    for i in range(9, len(cols)):
        geno = cols[i].split(":")
        #Extract DP
        dp = int(geno[2])
        #I get the ADf and ADr of the alternate allele
        adf = geno[3].split(",")[1]
        adr = geno[4].split(",")[1]
        #check if there is ADb
        if len(geno) < 6:
            adb = 0
        else:
            adb = geno[5].split(",")[1]
        ad = int(adf) + int(adr) + int(adb)
        #I have to avoid dividing by 0
        if dp == 0:
            cols[i] = str(ad)+":"+str(dp)
            continue
        #Calculate allele frequency
        af = ad/dp
        freqs.append(af)
        cols[i] = str(ad)+":"+str(dp)
    if not maf:
        print("\t".join(cols))
    else:
        if sum(freqs)/len(freqs) > maf:
            print("\t".join(cols))


def main():
    f = open(sys.argv[1])
    maf = float(sys.argv[2])
    for line in f:
        line = line.rstrip()
        if line[0] == "#":
            print(line)
        else:
            cols = line.split("\t")
            recode(cols, maf)
    f.close()

main()
