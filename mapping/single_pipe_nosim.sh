#!/usr/bin/env bash

print_message () {
	now=$(date +"%c")
	echo "[$1] $2 ($now)" >&2
}

pipe () {
	local i=$1
	mkdir "$i"
	print_message "1-$i" "Directory created"
	cd "$i"
	mkdir log
	cp "/data3/murillo/na_clines/seqs/raw_fqs/$i"* .
	pigz -d *".fastq.gz"
	print_message "2-$i" "Fastqs copied"
  cp "/data3/murillo/reference/dmel-all-chromosome-r6.15.fasta" ./dmel_ref.fasta
	cp "/data3/murillo/reference/dsim-all-chromosome-r2.02.fasta" ./dsim_ref.fasta
  bwa index dmel_ref.fasta
  print_message "3-$i" "Dmel genome copied and indexed"
  bbsplit.sh -Xmx32g t=1 ambiguous2=all in="$i.fastq" ref=dmel_ref.fasta,dsim_ref.fasta basename=out_%.fq outu=clean.fq refstats=stats.txt 2>log/"$i.bbsplit.log"
  cat clean.fq out_dmel_ref.fq | paste - - - - | sort --temporary-directory="/data3/murillo/tmp" -k1,1 -t " " | tr "\t" "\n" > merged.fastq 2> log/"$i.merged.log"
  print_message "4-$i" "Removed Dsim reads"
	bwa mem -t 1 -M -R "@RG\tID:$i.R01\tSM:$i" dmel_ref.fasta merged.fastq > "$i.sam" 2>log/"$i.sam.log"
	print_message "5-$i" "Reads were aligned to the reference genome"
	/usr/lib/jvm/java-8-oracle/jre/bin/java -Xmx32G -Djava.io.tmpdir=`pwd`/tmp -jar $PICARD SortSam INPUT="$i.sam" OUTPUT="$i.bam" SORT_ORDER=coordinate 2>log/"$i.bam.log"
	print_message "5-$i" ".sam converted into sorted .bam"
	samtools view -b -F 4 "$i.bam" > "$i"\_"mapped.bam" 2> log/"$i.mapped.log"
	/usr/lib/jvm/java-8-oracle/jre/bin/java -Xmx32G -Djava.io.tmpdir=`pwd`/tmp -jar $PICARD MarkDuplicates INPUT="$i"\_"mapped.bam" OUTPUT="$i.dedup.bam" METRICS_FILE=log/metrics.txt REMOVE_DUPLICATES=true ASSUME_SORTED=true 2> log/"$i.rmdup.log"
	print_message "6-$i" "Removed unmapped reads and duplicates from sorted .bam"
	/usr/lib/jvm/java-8-oracle/jre/bin/java -jar $PICARD BuildBamIndex INPUT="$i.dedup.bam"
	print_message "7-$i" "dedup.bam indexed"
	/usr/lib/jvm/java-8-oracle/jre/bin/java -jar $PICARD CreateSequenceDictionary R= dmel_ref.fasta O= dmel_ref.dict
	print_message "8-$i" "dmel_ref.fasta dictionary created"
	samtools faidx dmel_ref.fasta
	print_message "9-$i" ".fai for the dmel genome created"
	/usr/lib/jvm/java-8-oracle/jre/bin/java -jar $GATK -T RealignerTargetCreator -R dmel_ref.fasta -I "$i.dedup.bam" -o "$i.IndelRealigner.intervals" 2>log/GATK_RealignerTargetCreator.log
	print_message "10-$i" "Targets for the Realigner mapped"
	/usr/lib/jvm/java-8-oracle/jre/bin/java -Xmx32G -Djava.io.tmpdir=`pwd`/tmp -jar $GATK -T IndelRealigner -R dmel_ref.fasta -targetIntervals "$i.IndelRealigner.intervals" -I "$i.dedup.bam" -o "$i.inrealigned.bam" 2>log/GATK_IndelRealigner.log
	print_message "11-$i" "Local realignment performed"
	rm "$i.dedup.bam" &
	rm "$i.sam" &
	rm *".fastq" &
	rm *".fq" &
	cd ../
	print_message "13-$i" "Pipeline for $i is now complete."
}

accessions=( SRR1177951 SRR1177952 SRR1177953 SRR1177955 )

N=10
for i in "${accessions[@]}"
do
	# Every Nth job, stop and wait for everything to complete.
	if (( j % N == 0 )); then
		wait
	fi
	((j++))
	pipe "$i" &
done

