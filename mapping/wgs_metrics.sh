#!/usr/bin/env bash

wgs () {
	local i=$1
	cd "$i"
	/usr/lib/jvm/java-8-oracle/jre/bin/java -Xmx16G -D/usr/lib/jvm/java-8-oracle/jre/bin/java.io.tmpdir=/data3/murillo/tmp -jar $PICARD CollectWgsMetrics I="$i.inrealigned.bam" O="$i.wgs_metrics.txt" R=dmel_ref.fasta
	cd ../
}

#accessions=( SRR1173990 SRR1173991 SRR1177123 SRR1177950 SRR1525685 SRR1525694 SRR1525695 SRR1525696 SRR1525697 SRR1525698 SRR2006283 SRR1525768 SRR1525769 SRR1525770 SRR1525771 SRR1525772 SRR1525773 SRR3590551 SRR3590557 SRR3590560 SRR3590561 SRR3590563 SRR3939095 SRR3939096 SRR3939097 SRR3939098 SRR3939099 )

accessions=( SRR1177951 SRR1177952 SRR1177953 SRR1177955 )

#accessions=( SRR1173990 )

N=10
for i in "${accessions[@]}"
do
	# Every Nth job, stop and wait for everything to complete.
	if (( j % N == 0 )); then
		wait
	fi
	((j++))
	wgs "$i" &
done
