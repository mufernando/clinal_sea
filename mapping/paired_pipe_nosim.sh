#!/usr/bin/env bash

print_message () {
	now=$(date +"%c")
	echo "[$1] $2 ($now)" >&2
}

pipe () {
	local i=$1
	mkdir "$i"
	print_message "1-$i" "Directory created"
	cd "$i"
	mkdir log
	cp "/data3/murillo/na_clines/seqs/raw_fqs/$i"* .
	pigz -d *".fastq.gz"
	print_message "2-$i" "Fastqs copied"
  cp "/data3/murillo/reference/dmel-all-chromosome-r6.15.fasta" ./dmel_ref.fasta
	cp "/data3/murillo/reference/dsim-all-chromosome-r2.02.fasta" ./dsim_ref.fasta
  bwa index dmel_ref.fasta
  print_message "3-$i" "Dmel genome copied and indexed"
  bbsplit.sh -Xmx32g t=30 ambiguous2=all in1="$i"\_"1.fastq" in2="$i"\_"2.fastq" ref=dmel_ref.fasta,dsim_ref.fasta basename=out_%.fq outu1=clean1.fq outu2=clean2.fq refstats=stats.txt 2>log/"$i.bbsplit.log"
  cat clean1.fq clean2.fq out_dmel_ref.fq | paste - - - - | sort --temporary-directory="/data3/murillo/tmp" -k1,1 -t " " | tr "\t" "\n" > merged.fastq 2> log/"$i.merged.log"
  print_message "4-$i" "Removed Dsim reads"
	bwa mem -p -t 30 -M -R "@RG\tID:$i.R01\tSM:$i" dmel_ref.fasta merged.fastq > "$i.sam" 2>log/"$i.sam.log"
	print_message "5-$i" "Reads were aligned to the reference genome"
	/usr/lib/jvm/java-8-oracle/jre/bin/java -Xmx32G -Djava.io.tmpdir=`pwd`/tmp -jar $PICARD SortSam INPUT="$i.sam" OUTPUT="$i.bam" SORT_ORDER=coordinate 2>log/"$i.bam.log"
	print_message "5-$i" ".sam converted into sorted .bam"
	samtools view -b -F 4 "$i.bam" > "$i"\_"mapped.bam" 2> log/"$i.mapped.log"
	/usr/lib/jvm/java-8-oracle/jre/bin/java -Xmx32G -Djava.io.tmpdir=`pwd`/tmp -jar $PICARD MarkDuplicates INPUT="$i"\_"mapped.bam" OUTPUT="$i.dedup.bam" METRICS_FILE=log/metrics.txt REMOVE_DUPLICATES=true ASSUME_SORTED=true 2> log/"$i.rmdup.log"
	print_message "6-$i" "Removed unmapped reads and duplicates from sorted .bam"
	/usr/lib/jvm/java-8-oracle/jre/bin/java -jar $PICARD BuildBamIndex INPUT="$i.dedup.bam"
	print_message "7-$i" "dedup.bam indexed"
	/usr/lib/jvm/java-8-oracle/jre/bin/java -jar $PICARD CreateSequenceDictionary R= dmel_ref.fasta O= dmel_ref.dict
	print_message "8-$i" "dmel_ref.fasta dictionary created"
	samtools faidx dmel_ref.fasta
	print_message "9-$i" ".fai for the dmel genome created"
	/usr/lib/jvm/java-8-oracle/jre/bin/java -jar $GATK -T RealignerTargetCreator -R dmel_ref.fasta -I "$i.dedup.bam" -o "$i.IndelRealigner.intervals" 2>log/GATK_RealignerTargetCreator.log
	print_message "10-$i" "Targets for the Realigner mapped"
	/usr/lib/jvm/java-8-oracle/jre/bin/java -Xmx32G -Djava.io.tmpdir=`pwd`/tmp -jar $GATK -T IndelRealigner -R dmel_ref.fasta -targetIntervals "$i.IndelRealigner.intervals" -I "$i.dedup.bam" -o "$i.inrealigned.bam" 2>log/GATK_IndelRealigner.log
	print_message "11-$i" "Local realignment performed"
	rm "$i.dedup.bam" &
	rm "$i.sam" &
	rm *".fastq" &
	rm *".fq" &
	cd ../
	print_message "13-$i" "Pipeline for $i is now complete."
}

#accessions=( SRR1173990 SRR1173991 SRR1177123 SRR1177950 SRR1525685 SRR1525694 SRR1525695 SRR1525696 SRR1525697 SRR1525698 SRR2006283 SRR1525768 SRR1525769 SRR1525770 SRR1525771 SRR1525772 SRR1525773 SRR3590551 SRR3590557 SRR3590560 SRR3590561 SRR3590563 SRR3939095 SRR3939096 SRR3939097 SRR3939098 SRR3939099 )
accessions=( SRR3939097 )
N=10
for i in "${accessions[@]}"
do
	# Every Nth job, stop and wait for everything to complete.
	if (( j % N == 0 )); then
		wait
	fi
	((j++))
	pipe "$i" &
done

